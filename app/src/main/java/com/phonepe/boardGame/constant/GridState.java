package com.phonepe.boardGame.constant;

/**
 * Created by Kalpesh Patel on 29/03/19.
 */
public enum GridState {

    VISIBLE(0), FOLDED(1);

    int state = 0;

    GridState(int i) {
        state = i;
    }

    public int getState() {
        return state;
    }


}
