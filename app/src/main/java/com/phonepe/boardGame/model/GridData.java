package com.phonepe.boardGame.model;

import com.phonepe.boardGame.constant.GridState;

/**
 * Created by Kalpesh Patel on 29/03/19.
 */
public class GridData {

    public int imageNumber;
    public GridState gridState;

    public GridData(int imageNumber) {
        this.imageNumber = imageNumber;
        gridState = GridState.FOLDED;
    }

    public int getImageNumber() {
        return imageNumber;
    }

    public void setImageNumber(int imageNumber) {
        this.imageNumber = imageNumber;
    }

    public GridState getGridState() {
        return gridState;
    }

    public void setGridState(GridState gridState) {
        this.gridState = gridState;
    }
}
