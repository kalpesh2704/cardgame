package com.phonepe.boardGame.interfaces;

/**
 * Created by Kalpesh Patel on 29/03/19.
 */
public interface OnItemClick {
    void onItemClick(int ItemNumber);
}
