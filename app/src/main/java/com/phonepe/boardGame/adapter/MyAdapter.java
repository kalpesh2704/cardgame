package com.phonepe.boardGame.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phonepe.boardGame.R;
import com.phonepe.boardGame.interfaces.OnItemClick;
import com.phonepe.boardGame.model.GridData;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private final OnItemClick onItemClick;
    boolean folded;
    private ArrayList<GridData> gridData;

    public MyAdapter(ArrayList<GridData> gridData, OnItemClick onItemClick) {
        this.gridData = gridData;
        this.onItemClick = onItemClick;
    }

    public void setFolded(boolean folded) {
        this.folded = folded;
    }

    public ArrayList<GridData> getGridData() {
        return gridData;
    }

    public void setGridData(ArrayList<GridData> gridData) {
        this.gridData = gridData;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_view, parent, false);
        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.textView.setText(String.valueOf(gridData.get(position).getImageNumber()));
        switch (gridData.get(position).gridState) {
            case VISIBLE:
                holder.textView.setVisibility(View.VISIBLE);
                break;
        }

    }


    @Override
    public int getItemCount() {
        return gridData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView textView;
        private View view;

        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            textView = view.findViewById(R.id.customTextView);
        }

        @Override
        public void onClick(View view) {
            if (onItemClick != null) onItemClick.onItemClick(getAdapterPosition());
        }
    }
}