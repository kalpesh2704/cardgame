package com.phonepe.boardGame.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.phonepe.boardGame.AppUtils;


public class MySharedPrefs {
    public static final String BOARD_PREF = "boardPref";
    public static final String LEVEL = "level";
    public static final String SCORE = "score";


    private static MySharedPrefs mySharedPrefs;
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    private MySharedPrefs(Context context) {
        sharedPreferences = context.getSharedPreferences(BOARD_PREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static MySharedPrefs getInstance(Context context) {
        if (mySharedPrefs == null) {
            mySharedPrefs = new MySharedPrefs(context);
        }
        return mySharedPrefs;
    }

    private void commit() {
        editor.commit();
    }

    public void putString(String key, String value) {
        if (AppUtils.isStringEmpty(key)) {
            throw new IllegalArgumentException("Key is empty or null");
        }
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key) {
        String value = sharedPreferences.getString(key, null);
        if (AppUtils.isStringEmpty(value)) {
            return null;
        } else {
            return value;
        }
    }

    public void putBoolean(String key, boolean value) {
        if (AppUtils.isStringEmpty(key)) {
            throw new IllegalArgumentException("Key is empty or null");
        }
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void putInteger(String key, int value) {
        if (AppUtils.isStringEmpty(key)) {
            throw new IllegalArgumentException("Key is empty or null");
        }
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public void putLong(String key, long value) {
        if (AppUtils.isStringEmpty(key)) {
            throw new IllegalArgumentException("Key is empty or null");
        }
        editor.putLong(key, value);
        editor.commit();
    }

    public long getLong(String key) {
        return sharedPreferences.getLong(key, 0L);
    }

    public void putFloat(String key, float value) {
        if (AppUtils.isStringEmpty(key)) {
            throw new IllegalArgumentException("Key is empty or null");
        }
        editor.putFloat(key, value);
        editor.commit();
    }

    public float getFloat(String key) {
        return sharedPreferences.getFloat(key, 0.0f);
    }

    public interface keys {
        String PDF_VIEW_MODE_VERTICAL = "PDF_VIEW_MODE_VERTICAL";
        String IS_LOGGED_IN = "IS_LOGGED_IN";
    }
}
