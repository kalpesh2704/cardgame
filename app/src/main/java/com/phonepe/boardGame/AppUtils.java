package com.phonepe.boardGame;

/**
 * @author Kalpesh Patel
 */
public class AppUtils {


    /**
     * Checks strings is null/empty/length==0
     *
     * @param s
     * @return
     */
    public static boolean isStringEmpty(String s) {
        return (s == null) || (s.trim().equals(""));
    }


}
