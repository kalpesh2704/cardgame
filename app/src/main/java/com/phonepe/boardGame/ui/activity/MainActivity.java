package com.phonepe.boardGame.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.phonepe.boardGame.R;
import com.phonepe.boardGame.adapter.MyAdapter;
import com.phonepe.boardGame.constant.GridState;
import com.phonepe.boardGame.interfaces.OnItemClick;
import com.phonepe.boardGame.model.GridData;
import com.phonepe.boardGame.utils.MySharedPrefs;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements OnItemClick {

    int levels[][] = {{3, 2}, {4, 3}, {5, 4}};
    int timer[] = {60, 120, 180};
    int currentLevel = 0;
    private int score;
    private int totalCardsDone = 0;

    private boolean isGameInitiated;

    private GridData lastGridData;
    private int lastGridDataItem;
    private ArrayList<GridData> gridStates;
    private MyAdapter mAdapter;
    private int maxLevels = 2;
    private int elapsedTimeInSeconds = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initData();
        setListener();
        setRecyclerView();
    }


    private void setListener() {
        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab.setBackgroundResource(android.R.drawable.ic_media_pause);
                if (isGameInitiated) {
                    pauseTime();
                } else {
                    isGameInitiated = true;
                    showAllCards();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            foldAllCards();
                        }
                    }, 1000);
                }
            }
        });
    }

    private void foldAllCards() {


    }

    private void showAllCards() {

    }

    private void initData() {
        currentLevel = MySharedPrefs.getInstance(this).getInt(MySharedPrefs.LEVEL);
        score = MySharedPrefs.getInstance(this).getInt(MySharedPrefs.SCORE);
    }

    private void setRecyclerView() {

        RecyclerView recyclerView = findViewById(R.id.rvCards);

        final GridLayoutManager mLayoutManager = new GridLayoutManager(
                this, levels[currentLevel][1]);

        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(generateDate(), this);
        recyclerView.setAdapter(mAdapter);
    }

    private ArrayList<GridData> generateDate() {
        gridStates = new ArrayList<>();
        int data = 1;
        int loopSize = levels[currentLevel][0] * levels[currentLevel][1] / 2;
        for (int i = 0; i < loopSize; i++) {
            gridStates.add(new GridData(data));
            gridStates.add(new GridData(data++));
        }
        Collections.shuffle(gridStates);
        return gridStates;
    }

    @Override
    public void onItemClick(int ItemNumber) {

        if (lastGridData == null) {
            lastGridData = gridStates.get(ItemNumber);
            gridStates.get(ItemNumber).setGridState(GridState.VISIBLE);
            mAdapter.notifyItemChanged(ItemNumber);
            lastGridDataItem = ItemNumber;
        } else {
            if (lastGridData.getImageNumber() == gridStates.get(ItemNumber).getImageNumber()) {
                totalCardsDone += 2;
                gridStates.get(lastGridDataItem).setGridState(GridState.VISIBLE);
                gridStates.get(ItemNumber).setGridState(GridState.VISIBLE);
                mAdapter.setGridData(gridStates);
                mAdapter.notifyItemChanged(lastGridDataItem);
                mAdapter.notifyItemChanged(ItemNumber);
            } else {
                gridStates.get(lastGridDataItem).setGridState(GridState.FOLDED);
                gridStates.get(ItemNumber).setGridState(GridState.FOLDED);
                mAdapter.setGridData(gridStates);
                mAdapter.notifyItemChanged(lastGridDataItem);
                mAdapter.notifyItemChanged(ItemNumber);
            }
            lastGridData = null;
            if (totalCardsDone == levels[currentLevel][0] * levels[currentLevel][1]) {
                pauseTime();
                Toast.makeText(this, "Congratulation You Have Completed Level " + currentLevel, Toast.LENGTH_SHORT).show();
                MySharedPrefs.getInstance(this).putInteger(MySharedPrefs.LEVEL, currentLevel);
                calculateAndStoreScore();
                currentLevel++;
                if (currentLevel < maxLevels) {
                    Toast.makeText(this, "Congratulation You Have Completed All Levels", Toast.LENGTH_SHORT).show();
                    // Hide Recyclerview and show some item to exit game
                }
            }
        }
    }

    private void calculateAndStoreScore() {
        score += Math.round((elapsedTimeInSeconds * 20) / timer[currentLevel]);
        MySharedPrefs.getInstance(this).putInteger(MySharedPrefs.SCORE, score);
    }

    //This will Start TImer
    void startTimer() {

    }

    //This Will Pause Timer
    void pauseTime() {

    }
}
